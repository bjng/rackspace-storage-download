Select 
CONCAT_WS('/','photos',Concat('u',a.user_id),Concat('lst',a.listing_id),p.id,Concat('cretico_lst',a.listing_id,'_',p.id,'.jpg') )as pathIn,
CONCAT_WS('/',a.listing_id,CONCAT('cretico-',a.listing_id,'-',p.`order`,'-',p.id,'.jpg') ) as pathOut
from user_album a
join user_photos p
on a.uid = p.album_uid and p.`order`<=25
join listing l
on a.listing_id = l.id
where a.is_verified = 1 and l.visible ='v' and l.`status` = 'a'
order by a.listing_id,p.`order` asc