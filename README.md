# Install

Python 2.7 required

## Installing the App

1. run `python -m virtualenv env`
2. then activate sandbox environement with `source env/bin/activate`
3. `pip install -r requirements.txt`

Activating the environement makes sure that all python libs are taken from this project and not from global libs.


## Which files to download?
The app crawls download.csv, which includes the source and the target path.
All successfull downloads will be saved in downloads folder.

## Creating the CSV
The project also contains a inout sql for our database.
Run this SQL in workbench and save the result in the download.csv

## Where does the application start in the list?

... and what happens if the App crashes.
The last sucessful line index will be saved in idx.log.
If the app crashes the app will start again after restart at that index.
You can also change that index manually to start somewhere else in downloads.csv.

## How to start the app?

The app runs only on internal rackspace environement.
If you want to run the app locally, yo have to  comment the line in main.py that says `profile.set_interface('object-store', 'internal')`

If you havent activate the sandbox environement as described in "Installing the App", second point.
Then:

`python main.py`

